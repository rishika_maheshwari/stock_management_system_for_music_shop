#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <string.h>
#include <fstream>
#include <process.h>
#include <stdlib.h>

using namespace std;
//Class for items/product
class items
{
       int count;
       char serialno[30][30];
       char name[30][30];
       char price[30][30];
       char quantity[30][30];
public:
       void list_items(char);
       void add_items();
       void restock_items();
       void update_items();
       void order_items();
       void show_items();
       void sold_items();
};
//Function for add products
void items::add_items(){
    int pid,rate,quantity;
    string name;
    char input;
    P:
        cout << "Make your choice:\n[1] Add CD \n[2] Add DVD \n[3] Add Magazine \n[4] Add Books \n";
        cin>>input;
        cout<<"Enter Product id: ";
        cin>>pid;
        cout<<"\nEnter name of product:";
        cin>>name;
        cout<<"\nEnter price: ";
        cin>>rate;
        cout<<"\nEnter quantity: ";
        cin>>quantity;
        if(input == 49){
            fstream fin;
            fin.open("cd_files.txt", ios::app);
            fin<<pid<<";"<<name<<";"<<rate<<";"<<quantity<<";\n";
            fin.close();
        }
        else if(input == 50){
            fstream fin;
            fin.open("dvd_files.txt", ios::app);
            fin<<pid<<";"<<name<<";"<<rate<<";"<<quantity<<";\n";
            fin.close();
        }
        else if(input == 51){
            fstream fin;
            fin.open("magazine_files.txt", ios::app);
            fin<<pid<<";"<<name<<";"<<rate<<";"<<quantity<<";\n";
            fin.close();
        }
        else if(input == 52){
            fstream fin;
            fin.open("book_files.txt", ios::app);
            fin<<pid<<";"<<name<<";"<<rate<<";"<<quantity<<";\n";
            fin.close();
        }
        cout<<"\nProduct Successfully Added\n";
    char more;
    cout<<"Do You want to add more product (y/n) : ";
    cin>>more;
    if(more == 121){
        goto P;
    }
    else{
        return;
    }
}
//Function for changing the quantity
void items::restock_items(){
    char input;
    int found = 0;
    char p[30];
    ifstream fin;
    ofstream temp;
    cout << "Make your choice:\n[1] Restock CD's \n[2] Restock DVD's \n[3] Restock Magazine's \n[4] Restock Books \n\n";
    cin>>input;
    if(input == 49){
        list_items(input);
        fin.open("cd_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::out|ios::app);
        show_items();
    }
    else if(input == 50){
        list_items(input);
        fin.open("dvd_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::out|ios::app);
        show_items();
    }
    else if(input == 51){
        list_items(input);
        fin.open("magazine_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::out|ios::app);
        show_items();
    }
    else if(input == 52){
        list_items(input);
        fin.open("book_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::out|ios::app);
        show_items();
    }
    if(fin.fail() || (fin.peek() == EOF)){
        cout<<"File is Empty";
    }
    else{
        string quan;
        int v;
        cout<<"Enter the product id which you want to modify: \n";
        cin >> p;
        for (v = 0; v < count; v++)
        {
            if (strcmp(serialno[v], p) == 0)
            {
                found = 1;
                break;
            }
        }
        if(found == 1){
            string line;
            cout<<"Enter new quantity: \n";
            cin>>quan;
            string strTemp;
            string strReplace = quantity[v];
            string strNew = quan+";";
            while(fin >> strTemp)
            {
                if(strTemp.find(quantity[v]) != string::npos){
                    strTemp = strNew;
                    temp<<serialno[v]<<";"<<name[v]<<";"<<price[v]<<";";
                }
                temp << strTemp<<"\n";
            }

            temp.close();
            fin.close();
            switch(input){
                case 49:
                    remove("cd_files.txt");
                    rename("temp.txt","cd_files.txt");
                    break;
                case 50:
                    remove("dvd_files.txt");
                    rename("temp.txt","dvd_files.txt");
                case 51:
                    remove("magazine_files.txt");
                    rename("temp.txt","magazine_files.txt");
                case 52:
                    remove("book_files.txt");
                    rename("temp.txt","book_files.txt");
            }
            cout<<"Quantity Increased successfully\n";
        }
    }
}
//Function to delete/update any product
void items:: update_items(){
    char input;
    int found = 0;
    char p[30];
    ifstream fin;
    ofstream temp;
    cout << "Make your choice:\n[1] Delete CD's \n[2] Delete DVD's \n[3] Delete Magazine's \n[4] Delete Books \n\n";
    cin>>input;
    if(input == 49){
        list_items(input);
        fin.open("cd_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::binary);
        show_items();
    }
    else if(input == 50){
        list_items(input);
        fin.open("dvd_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::binary);
        show_items();
    }
    else if(input == 51){
        list_items(input);
        fin.open("magazine_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::binary);
        show_items();
    }
    else if(input == 52){
        list_items(input);
        fin.open("book_files.txt", ios::in|ios::binary|ios::app);
        temp.open("temp.txt", ios::binary);
        show_items();
    }
    if(fin.fail() || (fin.peek() == EOF)){
        cout<<"File is Empty";
    }
    else{
        int v;
        cout<<"Enter the product id which you want to delete: \n";
        cin >> p;
        for (v = 0; v < count; v++)
        {
            if (strcmp(serialno[v], p) == 0)
            {
                found = 1;
                break;
            }
        }
        if(found == 1){
            string strTemp;
            string strNew = " ";
            while(fin >> strTemp)
            {
                if(strTemp.find(serialno[v]) != string::npos){
                    strTemp = strNew;
                }
                temp << strTemp<<"\n";


            }
            cout<<"Deleted Successfully\n";
            temp.close();
            fin.close();
            switch(input){
                case 49:
                    remove("cd_files.txt");
                    rename("temp.txt","cd_files.txt");
                    break;
                case 50:
                    remove("dvd_files.txt");
                    rename("temp.txt","dvd_files.txt");
                case 51:
                    remove("magazine_files.txt");
                    rename("temp.txt","magazine_files.txt");
                case 52:
                    remove("book_files.txt");
                    rename("temp.txt","book_files.txt");
            }
        }
    }
}
//Function For order products
void items::order_items()
{
    A:
        fstream fout;
        char input;
        cout << "Make your choice:\n[1] Order CD \n[2] Order DVD \n[3] Order Magazine \n[4] Order Books \n";
        cin>>input;
        if(input == 49){
           list_items(input);
           fout.open("cd_files.txt", ios::in);
           show_items();
        }
        else if(input == 50){
           list_items(input);
           fout.open("dvd_files.txt", ios::in);
           show_items();
        }
        else if(input == 51){
           list_items(input);
           fout.open("magazine_files.txt", ios::in);
           show_items();
       }
       else if(input == 52){
           list_items(input);
          fout.open("book_files.txt", ios::in);
          show_items();
       }
       char pid[30];
       long qid;
       int found = 0;

       if(fout.fail() || (fout.peek() == EOF)){
            cout<<"File is Empty\n";
       }
       else{
           cout << "Enter Item id: ";
           cin >> pid;
           int v;
           for (v = 0; v < count; v++)
           {
                  if (strcmp(serialno[v], pid) == 0)
                  {
                         found = 1;
                         break;
                  }
           }
           if (found == 1)
           {
                  char read;
                  cout << "Enter quantity: ";
                  cin >> qid;
           AA:
                  cout << "Are you sure of want to order " << qid << " quantity of " << name[v];
                  cout << "\nEnter Yes/No (y/n) to confirm the order : ";
                  cin >> read;
                  if (read == 121)
                  {
                        long quan = atoi(quantity[v]) - qid;
                        fstream fin;
                        fin.open("sold_items.txt", ios::app);
                        fin<<serialno[v]<<";"<<name[v]<<";"<<price[v]<<";"<<qid<<";\n";
                        fin.close();
                        cout<<"\nRemaining vales:\n";
                        cout << serialno[v] << ";";
                        cout << name[v] << ";";
                        cout << price[v] << ";";
                        cout << quan << ";" << "\n";
                        cout << "\nOrder Placed Successfully\n" << qid << " taken from the shop\n";
                        ofstream temp;
                        temp.open("temp.txt", ios::binary);
                        string strTemp;
                        string strReplace = quantity[v];
                        string strNew = to_string(quan)+";";
                        while(fout >> strTemp)
                        {
                            if(strTemp.find(quantity[v]) != string::npos){
                                strTemp = strNew;
                                temp<<serialno[v]<<";"<<name[v]<<";"<<price[v]<<";";
                            }
                            temp << strTemp<<"\n";
                        }

                        temp.close();
                        fout.close();
                        switch(input){
                            case 49:
                                remove("cd_files.txt");
                                rename("temp.txt","cd_files.txt");
                                break;
                            case 50:
                                remove("dvd_files.txt");
                                rename("temp.txt","dvd_files.txt");
                                break;
                            case 51:
                                remove("magazine_files.txt");
                                rename("temp.txt","magazine_files.txt");
                                break;
                            case 52:
                                remove("book_files.txt");
                                rename("temp.txt","book_files.txt");
                                break;
                        }
                  }
                  else if (read == 110){
                    cout<<"You didn't place any order\n";
                  }
                  else
                  {
                     system("cls");
                     goto AA;
                  }
                  char more;
                  cout<<"would you like to buy something else (Y/N): ";
                  cin>>more;
                  if(more == 121){
                    goto A;
                  }
                  else if(more == 110){
                    return;
                  }
           }

       }


}
//Function for listing the products
void items::list_items(char n)
{
       char str[2000];
       ifstream fin;
       if(n == 49){
         fin.open("cd_files.txt", ofstream::in);
       }
       else if(n == 50){
         fin.open("dvd_files.txt", ofstream::in);
       }
       else if(n == 51){
         fin.open("magazine_files.txt", ofstream::in);
       }
       else if(n == 52){
         fin.open("book_files.txt", ofstream::in);
       }
       else if(n == 115){
        fin.open("sold_items.txt", ofstream::in);
       }
       count = 0;
           while (fin >> str)
           {
               int savei = 0;
                  int i = 0;
                  int d = 0;
                  while (i < strlen(str))
                  {
                      if (str[i] == ';')
                         {
                               char s[30];
                               int l = 0;
                               for (int j = savei; j < i; j++)
                               {
                                      s[l] = str[j];
                                      l++;
                               }
                               s[l] = '\0';
                               if (d == 0)
                                      strcpy(serialno[count], s);
                               else if (d == 1)
                                      strcpy(name[count], s);
                               else if (d == 2)
                                      strcpy(price[count], s);
                               else if (d == 3)
                                      strcpy(quantity[count], s);
                               d++;

                               i++;
                               savei = i;
                         }

                         i++;

                  }
                  count++;
                  if (fin.eof())
                         break;
           }
           fin.close();
}


//Function for showing the listing
void items::show_items()
{
       cout << "p-id\t\t\t name\t\t\t\t  price\t\t\t\t  quantity\n";
       cout << "---------------------------------------------------------------------------------------------------------\n";
       for (int v = 0; v < count; v++)
       {
              printf("%s", serialno[v]);
              printf("\t\t\t%8s", name[v]);
              printf("\t\t\t%8s", price[v]);
             // printf("%15s", quality[v]);
              printf("\t\t\t%10s", quantity[v]);
              printf("\n");
       }
}
//Function to view our sales
void items::sold_items(){
    list_items(115);
    show_items();
}
//Main Function
int main()
{
A:
       system("cls");
       cout << "Make your choice:\n[1] Add new product\n[2] Order products\n[3] Restock product \n[4] Delete product \n[5] View Report of sales \n[6] For quit\n\n";
       items ob;
       char a;
       cin >> a;
       system("cls");
       if (a == 49)
             ob.add_items();
       else if (a == 50)
             ob.order_items();
        else if(a== 51)
              ob.restock_items();
        else if(a == 52)
             ob.update_items();
        else if(a == 53)
            ob.sold_items();
        else if (a == 54)
            exit(0);
       else
              goto A;
       cout << "\n\nPress any key to return to Main Menu.";
       getch();
       goto A;

}
